import argparse

import pandas as pd


# 定义统计重复值的函数
def check_duplicates(csv_file, column_name):
    # 读取 CSV 文件
    df = pd.read_csv(csv_file)

    # 检查字段是否存在
    if column_name not in df.columns:
        print(f"错误：CSV 文件中没有字段 '{column_name}'")
        return

    # 查看是否有重复的值
    has_duplicates = df[column_name].duplicated().any()

    if has_duplicates:
        print(f"字段 '{column_name}' 有重复值。")
    else:
        print(f"字段 '{column_name}' 没有重复值。")

    # 统计字段频次
    value_counts = df[column_name].value_counts()
    duplicates = value_counts[value_counts > 1]

    if not duplicates.empty:
        print(f"\n重复值及其出现次数：")
        print(duplicates)
    else:
        print(f"\n没有重复值。")


# 设置命令行参数
def parse_args():
    parser = argparse.ArgumentParser(description="统计 CSV 文件某个字段的重复值及其频次")
    parser.add_argument('csv_file', help="CSV 文件路径")
    parser.add_argument('column_name', help="要统计的字段名称")
    return parser.parse_args()


# 主函数
def main():
    args = parse_args()
    check_duplicates(args.csv_file, args.column_name)


if __name__ == '__main__':
    main()
