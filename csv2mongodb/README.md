# 根据CSV文件导入数据到MongoDB例子

```bash
pip3 install pymongo pandas py-snowflake-id
```

使用虚拟环境

```bash
# 首次，在当前目录创建虚拟环境
python3 -m venv myenv
# 激活虚拟环境(fish shell，根据实际环境选择activate脚本)
source myenv/bin/activate.fish
# 安装所需包
python3 -m pip install pymongo pandas py-snowflake-id

# 非首次，激活环境
source myenv/bin/activate.fish
```

实现步骤

1. 解析 CSV 文件
使用 pandas 读取 CSV 文件，方便数据操作。

2. 生成雪花 ID
使用 snowflake-id 库或实现自定义雪花 ID 生成逻辑。

3. 导入 MongoDB
使用 PyMongo 连接到 MongoDB 并插入数据。

## 配置

配置文件 `config.ini`，配置 MongoDB 连接信息和 CSV 文件信息。

例子:

```ini
[MongoDB]
uri = mongodb://123.123.123.23:8635
database = test
collection = test

[CSV]
; CSV日期类型的表头名
date_columns = createDatetime,updateDatetime
; 过滤的无效列的表头名
columns_to_ignore = SNO,OTHER_M_ID,OIL_M_ID
```
