#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
from sys import argv

from amdmp_base import BaseTestData

# from testdata.testdata_tools import TestUtils, DbThreadRunner

sys.path.append('../')
from testdata_tools import TestUtils, DbThreadRunner, SqlBuilder

db_name = 'yundt_shop_trade'
tb_name = 'tr_order'

# 测试数据量
test_data_target_size = 10
# 最大线程数
max_threads = 6


class TrOrder(BaseTestData):
    # Class variable
    order_type = 1
    pay_status = 0
    pay_amount = 1
    total_item_price = pay_amount
    item_total = pay_amount
    member_id = 1000
    member_account = 'pytestdata'
    member_mobile = '13800138000'
    order_group_no = 'A003'

    def __init__(self, id_, order_no):
        BaseTestData.__init__(self)
        # instance variable
        self.id = id_
        self.order_no = order_no


# 构造order数据
def generate_test_datas(size=10):
    test_datas = []
    for _ in range(size):
        _id = TestUtils.gen_id()
        _order_no = TestUtils.gen_order_no()

        test_data_obj = TrOrder(_id, _order_no)

        test_datas.append(test_data_obj)

    return test_datas


def get_sqlbuilder(tb_name_=None, size=None):
    test_datas = generate_test_datas(size)
    # 打印生成的数据
    # for test_data in test_datas:
    #     print(test_data.__dict__)

    sql_builder = SqlBuilder(tb_name_, test_datas)
    return sql_builder


if __name__ == '__main__':

    # 记录开始时间
    start_time = time.time()

    if len(argv) > 1:
        if not argv[1].isdigit():
            exit("test_data_target_size must be digit")
        test_data_target_size = int(argv[1])

    if len(argv) > 2:
        if not argv[2].isdigit():
            exit("max_threads must be digit")
        max_threads = int(argv[2])

    if test_data_target_size < max_threads:
        exit("test_data_target_size must be greater than max_threads")

    print("生成数据数量: %s,线程数: %s" % (test_data_target_size, max_threads))

    db_runner = DbThreadRunner(test_data_target_size, max_threads, db_name, tb_name)
    # 多线程执行数据任务
    db_runner.run_task(task_func=get_sqlbuilder)

    # 记录结束时间
    end_time = time.time()

    # 计算执行时间
    execution_time = end_time - start_time

    # 打印执行时间
    print(f"执行时间：{execution_time} 秒")
