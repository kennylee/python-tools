#!/usr/bin/env python
# -*- coding: utf-8 -*-

class BaseTestData:
    def __init__(self):
        self.instance_id = -1
        self.create_person = 'pytestdata'
        self.create_time = '1971-01-01 00:00:00'
        self.update_person = self.create_person
        self.update_time = '1971-01-01 00:00:00'
        self.tenant_id = -1
