# 边学习Python边做的一些小工具脚本

 * tomcat_download.py

    用来下载最新版本tomcat的脚本, 原意其实在Tomcat的Archives下载是最好的, 但国内速度不堪入目。
    而使用mirrors的速度也是有快有慢, 并且仅仅保留最新的版本。
    因为以上两个原因, 所以有了这个工具...

 * health