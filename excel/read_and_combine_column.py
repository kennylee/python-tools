#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd


def read_and_combine_column(file_path, sheet_index, column_index):
    # 读取Excel文件
    try:
        df = pd.read_excel(file_path, sheet_name=sheet_index, skiprows=1)
    except pd.errors.EmptyDataError:
        print("Excel文件为空")
        return None
    except FileNotFoundError:
        print("找不到指定的Excel文件")
        return None

        # 获取指定列的数据
    column_data_before = df.iloc[:, column_index]

    # 输出去重前的数据长度
    print("去重前数据长度:", len(column_data_before))

    # 获取去重后的数据并输出去重后的数据长度
    column_data_after = column_data_before.drop_duplicates()
    print("去重后数据长度:", len(column_data_after))

    # 将去重后的数据组合成字符串，每个值两边加上单引号，并在值之间用逗号分隔
    combined_string = ",".join(f"'{value}'" for value in column_data_after)

    return combined_string


# 例子：读取Excel文件中第一个工作表的第一列数据并组合成字符串（去除重复值）
file_path = "/Users/kennylee/Downloads/1220传祺超伙留资-门店被刷.xlsx"  # 替换成你的Excel文件路径
sheet_index = 0  # 工作表索引
column_index = 5  # 读取的列索引

result = read_and_combine_column(file_path, sheet_index, column_index)

if result is not None:
    print("组合后的字符串:\n", result)
